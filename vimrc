" general {{{
" use indentation for folds
set foldmethod=indent
set foldnestmax=5
set foldlevelstart=99
set foldcolumn=0

augroup vimrcFold
  " fold vimrc itself by categories
  autocmd!
  autocmd FileType vim set foldmethod=marker
  autocmd FileType vim set foldlevel=0
augroup END

" Sets how many lines of history VIM has to remember
set history=700

" Set to auto read when a file is changed from the outside
set autoread

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
if ! exists("mapleader")
  let mapleader = ","
endif

if ! exists("g:mapleader")
  let g:mapleader = ","
endif

" Leader key timeout
set tm=2000

" Allow the normal use of "," by pressing it twice
noremap ,, ,

" Use par for prettier line formatting
set formatprg="PARINIT='rTbgqR B=.,?_A_a Q=_s>|' par\ -w72"

" Kill the damned Ex mode.
nnoremap Q <nop>

" Make <c-h> work like <c-h> again (this is a problem with libterm)
if has('nvim')
  nnoremap <BS> <C-w>h
endif


" }}}

" dein {{{

" Required:
"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/home/r892107/.vim/bundles/repos/github.com/Shougo/dein.vim
let bundlespath='/home/r892107/.vim/bundles'
let deinpath='/home/r892107/.vim/bundles/repos/github.com/Shougo/dein.vim'

" Required:
if dein#load_state(bundlespath)
  call dein#begin(bundlespath)

  " Let dein manage dein
  " Required:
  call dein#add(deinpath)

  " Add or remove your plugins here:
  " support bundles {{{
  call dein#add('nathanaelkane/vim-indent-guides')
  call dein#add('vim-scripts/gitignore')
  call dein#add('ynkdir/vim-vimlparser')
  call dein#add('syngan/vim-vimlint')
  call dein#add('neomake/neomake')
  " }}}
  " git {{{
  call dein#add('tpope/vim-fugitive')
  call dein#add('int3/vim-extradite')
  call dein#add('airblade/vim-gitgutter')
  " }}}
  " search {{{
  call dein#add('Shougo/denite.nvim')
  " }}}
  " bars, panels, files {{{
  call dein#add('bling/vim-airline')
  call dein#add('edkolev/tmuxline.vim')
  call dein#add('edkolev/promptline.vim')
  " }}}
  " text manipulation {{{
  call dein#add('vim-scripts/Align')
  call dein#add('simnalamburt/vim-mundo')
  call dein#add('scrooloose/nerdcommenter')
  call dein#add('godlygeek/tabular')
  call dein#add('tpope/vim-surround')
  call dein#add('Raimondi/delimitMate')
  " }}}
  " completion {{{
  call dein#add('Shougo/neocomplete.vim')
  "call dein#add('Shougo/deoplete.nvim')
  call dein#add('Shougo/neosnippet')
  call dein#add('Shougo/neosnippet-snippets')
  call dein#add('honza/vim-snippets')
  " }}}
  " move between tmux and vim panes {{{
  call dein#add('christoomey/vim-tmux-navigator')
  " }}}
  " csv {{{
  call dein#add('chrisbra/csv.vim')
  " }}}
  " Javascript/Typescript {{{
  call dein#add('HerringtonDarkholme/yats.vim')
  call dein#add('Quramy/tsuquyomi')
  call dein#add('Quramy/vim-js-pretty-template')
  call dein#add('pangloss/vim-javascript')
  call dein#add('mxw/vim-jsx')
  call dein#add('jason0x43/vim-js-indent')
  call dein#add('elzr/vim-json')
  call dein#add('1995eaton/vim-better-javascript-completion')
  call dein#add('othree/jspc.vim')
  call dein#add('ternjs/tern_for_vim')
  call dein#add('ramitos/jsctags')
  " }}}
  " Haskell {{{
  call dein#add('neovimhaskell/haskell-vim')
  call dein#add('enomsg/vim-haskellConcealPlus')
  call dein#add('eagletmt/ghcmod-vim')
  call dein#add('bitc/vim-hdevtools')
  call dein#add('eagletmt/neco-ghc')
  call dein#add('Twinside/vim-hoogle')
  " }}}
  " scala {{{
  call dein#add('derekwyatt/vim-scala')
  call dein#add('ensime/ensime-vim')
  " }}}
  " csharp {{{
  call dein#add('tpope/vim-dispatch')
  call dein#add('OmniSharp/omnisharp-roslyn')
  call dein#add('OmniSharp/omnisharp-vim')
  call dein#add('OrangeT/vim-csharp')
  " }}}
  " colorscheme {{{
  call dein#add('frankier/neovim-colors-solarized-truecolor-only')
  " }}}
  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

"End dein Scripts-------------------------

" }}}

" vim ui {{{

" set 7 lines to the cursor when movin vertically with j/k
set so=7

" turn on the WiLd menu
set wildmenu
" tab-complete files up to longest unambiguous prefix
set wildmode=list:longest,full

" always show current position
set ruler
set number

" show trailing whitespace
set list

" but only interesting whitespace
if &listchars ==# 'eol:$'
    set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

" treat *.gradle files as groovy file for syntax highlights
au BufNewFile,BufRead *.gradle setf groovy

" remove trailing whitespaces when writing a buffer
au BufWritePre vim DeleteTrailingWS()

" height of the command bar
set cmdheight=1

" always show statusline
set laststatus=2

" tame backspace to expected behvior
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" ignore case when searching
set ignorecase

" be smart when searching
set smartcase

" highlight search results
set hlsearch

" search like in modern browsers
set incsearch

" don't redraw while executing macros (better performance)
set lazyredraw

" magic for regex
set magic

" show matching brackets when text indicator is over them
set showmatch
" tenths of second to show matching brackets
set mat=2

" no sound on errors
set noerrorbells
set vb t_vb=

if &term =~ '256color'
  " disable Background Color Erase (BCE) so that color schemes
  " render properly when inside 256-color tmux and GNU screen.
  " see also http://snk.tuxfamily.org/log/vim-256color-bce.htmli
  set t_ut=
endif

" force redraw
map <silent> <leader>r :redraw!<cr>

" turn mouse mode on
nnoremap <leader>ma :set mouse=a<cr>
" turn mouse mode off
nnoremap <leader>mo :set mouse=<cr>

" default to mouse mode on
set mouse=a

" }}}

" colors and fonts {{{

try
  colorscheme solarized
  set background=dark
  if has("termguicolors")
    set termguicolors
  endif
  set t_8f=[38;2;%lu;%lu;%lum
  set t_8b=[48;2;%lu;%lu;%lum
  syntax on
catch
endtry

"" adjust signscolumn and syntastic to match colorscheme
"hi! link SignColumn LineNr
"hi! link SyntasticErrorSign ErrorMsg
"hi! link SyntasticWarningSign WarningMsg

" Use pleasant but very visible search hilighting
hi Search ctermfg=white ctermbg=173 cterm=none guifg=#ffffff guibg=#e5786d gui=none
hi! link Visual Search

" searing red very visible cursor
hi Cursor guibg=red

" Use same color behind concealed unicode characters
hi clear Conceal

" Don't blink normal mode cursor
set guicursor=n-v-c:block-Cursor
set guicursor+=n-v-c:blinkon0

" Set extra options when running in GUI mode
if has("gui_running")
  set guioptions-=T
  set guioptions-=e
  set guitablabel=%M\ %t
  if has("gui_win32")
    set guifont=Droid_Sans_Mono_Dotted_for_Powe:h8:cANSI
  endif
endif

" Set utf8 as standard encoding and en_US as the standard language
if !has('nvim')
  " Only set this for vim, since neovim is utf8 as default and setting it
  " causes problems when reloading the .vimrc configuration
  set encoding=utf8
  set fileencoding=utf8
endif

" use unix as the standard file type
set ffs=unix,dos,mac

" use powerline fonts for airline
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_symbols.space = "\ua0"

" }}}

" Files, backups and undo {{{

" Turn backup off, since most stuff is in Git anyway...
set nobackup
set nowb
set noswapfile

" Source the vimrc file after saving it
augroup sourcing
  autocmd!
  if has('nvim')
    autocmd bufwritepost init.vim source $MYVIMRC
  else
    autocmd bufwritepost .vimrc source $MYVIMRC
  endif
augroup END

" Open file prompt with current path
nmap <leader>e :e <C-R>=expand("%:p:h") . '/'<CR>

" Show undo tree
nmap <silent> <leader>u :GundoToggle<CR>

" Fuzzy find files

" }}}

" Text, tab and indent related {{{

" Use spaces instead of tabs
set expandtab

" Use this number of spaces for automatic indentation
set shiftwidth=4
set tabstop=4
set softtabstop=4

" Be smart when using tabs ;)
set smarttab

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" Copy and paste to os clipboard
nmap <leader>y "*y
vmap <leader>y "*y
nmap <leader>d "*d
vmap <leader>d "*d
nmap <leader>p "*p
vmap <leader>p "*p

" set tab width based on file type
autocmd FileType gradle setlocal sw=2 ts=2 sts=2
autocmd FileType groovy setlocal sw=2 ts=2 sts=2
autocmd FileType xml    setlocal sw=2 ts=2 sts=2
autocmd FileType java   setlocal sw=2 ts=2 sts=2

" }}}

" Search {{{

 " Fuzzy find files
nnoremap <silent> <leader><space> :Denite file_rec line<cr>
nnoremap <silent> <leader>/ :Denite grep:.<cr>

" Change file_rec command.
call denite#custom#var('file_rec', 'command',
  \ ['ag', '--follow', '--nocolor', '--nogroup', '-g', ''])
" Change mappings.
call denite#custom#map(
  \ 'insert',
  \ '<C-j>',
  \ '<denite:move_to_next_line>',
  \ 'noremap'
  \)
call denite#custom#map(
  \ 'insert',
  \ '<C-k>',
  \ '<denite:move_to_previous_line>',
  \ 'noremap'
  \)
" Change matchers.
call denite#custom#source(
  \ 'file_mru', 'matchers', ['matcher_fuzzy', 'matcher_project_files'])
" Change sorters.
call denite#custom#source(
  \ 'file_rec', 'sorters', ['sorter_sublime'])
" Ag command on grep source
call denite#custom#var('grep', 'command', ['ag'])
call denite#custom#var('grep', 'default_opts',
  \ ['-i', '--vimgrep'])
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', [])
call denite#custom#var('grep', 'separator', ['--'])
call denite#custom#var('grep', 'final_opts', [])
" Define alias
call denite#custom#alias('source', 'file_rec/git', 'file_rec')
call denite#custom#var('file_rec/git', 'command',
  \ ['git', 'ls-files', '-co', '--exclude-standard'])

" Change default prompt
call denite#custom#option('default', 'prompt', '')

" Change ignore_globs
call denite#custom#filter('matcher_ignore_globs', 'ignore_globs',
  \ [ '.git/', '.ropeproject/', '__pycache__/',
  \ 'venv/', 'images/', '*.min.*', 'img/', 'fonts/'])
" }}}

" visual mode related {{{

" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :call VisualSelection('f', '')<CR>
vnoremap <silent> # :call VisualSelection('b', '')<CR>

" }}}

" Moving around, tabs, windows and buffers {{{

" Treat long lines as break lines (useful when moving around in them)
nnoremap j gj
nnoremap k gk

nnoremap <c-h> <c-w>h
nnoremap <c-k> <c-w>k
nnoremap <c-j> <c-w>j
nnoremap <c-l> <c-w>l

" Disable highlight when <leader><cr> is pressed
" but preserve cursor coloring
nmap <silent> <leader><cr> :noh\|hi Cursor guibg=red<cr>

" Return to last edit position when opening files (You want this!)
augroup last_edit
  autocmd!
  autocmd BufReadPost *
       \ if line("'\"") > 0 && line("'\"") <= line("$") |
       \   exe "normal! g`\"" |
       \ endif
augroup END
" Remember info about open buffers on close
set viminfo^=%

" Open window splits in various places
nmap <leader>sh :leftabove  vnew<cr>
nmap <leader>sl :rightbelow vnew<cr>
nmap <leader>sk :leftabove  new<cr>
nmap <leader>sj :rightbelow new<cr>

" Manually create key mappings (to avoid rebinding C-\)
let g:tmux_navigator_no_mappings = 1

nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
nnoremap <silent> <c-\> :TmuxNavigatePrevious<cr>

" don't close buffers when you aren't displaying them
set hidden

" previous buffer, next buffer
nnoremap <leader>bp :bp<cr>
nnoremap <leader>bn :bn<cr>

" close every window in current tabview but the current
nnoremap <leader>bo <c-w>o

" delete buffer without closing pane
noremap <leader>bd :Bd<cr>

" }}}

" whitespace {{{

" Delete trailing white space on save
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc

" }}}

" Spell checking {{{

" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" }}}

" Helper functions {{{

function! CmdLine(str)
  exe "menu Foo.Bar :" . a:str
  emenu Foo.Bar
  unmenu Foo
endfunction

function! VisualSelection(direction, extra_filter) range
  let l:saved_reg = @"
  execute "normal! vgvy"

  let l:pattern = escape(@", '\\/.*$^~[]')
  let l:pattern = substitute(l:pattern, "\n$", "", "")

  if a:direction == 'b'
    execute "normal ?" . l:pattern . "^M"
  elseif a:direction == 'gv'
    call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.' . a:extra_filter)
  elseif a:direction == 'replace'
    call CmdLine("%s" . '/'. l:pattern . '/')
  elseif a:direction == 'f'
    execute "normal /" . l:pattern . "^M"
  endif

  let @/ = l:pattern
  let @" = l:saved_reg
endfunction

" }}}

" Slime {{{

vmap <silent> <leader>rs <Plug>SendSelectionToTmux
nmap <silent> <leader>rs <Plug>NormalModeSendToTmux
nmap <silent> <leader>rv <Plug>SetTmuxVars

" }}}

" Alignment {{{

" Stop Align plugin from forcing its mappings on us
let g:loaded_AlignMapsPlugin=1
" Align on equal signs
map <Leader>a= :Align =<CR>
" Align on commas
map <Leader>a, :Align ,<CR>
" Align on pipes
map <Leader>a<bar> :Align <bar><CR>
" Prompt for align character
map <leader>ap :Align

" }}}

" Git {{{

let g:extradite_width = 60
" Hide messy Ggrep output and copen automatically
function! NonintrusiveGitGrep(term)
  execute "copen"
  " Map 't' to open selected item in new tab
  execute "nnoremap <silent> <buffer> t <C-W><CR><C-W>T"
  execute "silent! Ggrep " . a:term
  execute "redraw!"
endfunction

command! -nargs=1 GGrep call NonintrusiveGitGrep(<q-args>)
nmap <leader>gs :Gstatus<CR>
nmap <leader>gg :copen<CR>:GGrep
nmap <leader>gl :Extradite!<CR>
nmap <leader>gd :Gdiff<CR>
nmap <leader>gb :Gblame<CR>

function! CommittedFiles()
  " Clear quickfix list
  let qf_list = []
  " Find files committed in HEAD
  let git_output = system("git diff-tree --no-commit-id --name-only -r HEAD\n")
  for committed_file in split(git_output, "\n")
    let qf_item = {'filename': committed_file}
    call add(qf_list, qf_item)
  endfor
  " Fill quickfix list with them
  call setqflist(qf_list, ' ')
endfunction

" Show list of last-committed files
nnoremap <silent> <leader>g? :call CommittedFiles()<CR>:copen<CR>

" }}}

" completion {{{

" disable AutoComplPop.
let g:acp_enableAtStartup = 0
" use neocomplete.
let g:neocomplete#enable_at_startup = 1
"let g:deoplete#enable_at_startup = 1
" use smartcase.
let g:neocomplete#enable_smart_case = 1
" set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 2
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" plugin key-mappings.
inoremap <expr><c-g>     neocomplete#undo_completion()
inoremap <expr><c-l>     neocomplete#complete_common_string()

" recommended key-mappings.
" <cr>: close popup and save indent.
inoremap <silent> <cr> <c-r>=<SID>my_cr_function()<cr>
function! s:my_cr_function()
  return (pumvisible() ? "\<c-y>" : "" ) . "\<cr>"
  " For no inserting <cr> key.
  "return pumvisible() ? "\<c-y>" : "\<cr>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<c-n>" : "\<TAB>"
" <c-h>, <BS>: close popup and delete backword char.
inoremap <expr><c-h> neocomplete#smart_close_popup()."\<c-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<c-h>"

" enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=tern#Complete
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif

if !exists('g:neocomplete#sources#omni#functions')
  let g:neocomplete#sources#omni#functions = {}
endif

if !exists('g:neocomplete#force_omni_input_patterns')
  let g:neocomplete#force_omni_input_patterns = {}
endif

let g:EclimCompletionMethod = 'omnifunc'
"let g:EclimCValidate = 0
"let g:EclimJavaValidate = 0
let g:EclimFileTypeValidate = 1
let g:neocomplete#sources#omni#input_patterns.java =
  \ '\%(\h\w*\|)\)\.\w*'
let g:neocomplete#force_omni_input_patterns.javascript = '[^. \t]\.\w*'

" javascript
let g:neocomplete#sources#omni#functions.javascript = [
    \ 'jspc#omni', 
    \ 'tern#Complete' 
    \ ]

autocmd BufWritePost *.scala silent :EnTypeCheck
" }}}

" snippets {{{

" plugin key-mappings.
imap <c-k> <Plug>(neosnippet_expand_or_jump)
smap <c-k> <Plug>(neosnippet_expand_or_jump)
xmap <c-k> <Plug>(neosnippet_expand_target)

" superTab like snippets behavior.
"imap <expr><TAB>
"  \ pumvisible() ? "\<c-n>" :
"  \ neosnippet#expandable_or_jumpable() ?
"  \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
  \ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

let g:neosnippet#enable_snipmate_compatibility = 1
let g:neosnippet#snippets_directory='~/.vim/bundle/vim-snippets/snippets'

" }}}

" promptline {{{

let g:promptline_preset = {
        \'a' : [ promptline#slices#user() ],
        \'b' : [ promptline#slices#cwd({'dir_limit': 3 }) ],
        \'c' : [ promptline#slices#vcs_branch(), '$(git rev-parse --short HEAD 2>/dev/null)' ],
        \'x' : [ promptline#slices#git_status() ],
        \'warn' : [ promptline#slices#last_exit_code() ]}
" }}}

" omnisharp {{{

let g:OmniSharp_server_type = 'roslyn'
let g:OmniSharp_selector_ui = 'unite'  " Use unite.vim

" }}}

let g:easytags_async = 1

au FileType xml exe ":silent %!xmllint --format --recover - 2>/dev/null"

nnoremap <F2> :mksession! ~/.vim_session<CR>
nnoremap <F3> :source ~/.vim_session<CR>

