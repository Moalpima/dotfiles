# Path to your oh-my-zsh installation.
if [[ $USER == "martin" ]]; then
  export ZSH=/home/martin/.oh-my-zsh
fi
if [[ $USER =~ "[rR]892107" ]]; then
  export ZSH=/home/r892107/.oh-my-zsh
fi

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="robbyrussell"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=2

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
if [[ "$(expr substr $(uname -s) 1 6)" == "CYGWIN" ]]; then
  plugins=(ant bower cabal colored-man-pages cp command-not-found gitfast git-extras git-flow-avh gradle history-substring-search jsontools mvn scala sbt tmux vagrant vi-mode zsh-syntax-highlighting)
  alias kitchen='/cygdrive/c/opscode/chefdk/embedded/bin/ruby C:/opscode/chefdk/bin/kitchen'
  alias knife='/cygdrive/c/opscode/chefdk/embedded/bin/ruby C:/opscode/chefdk/bin/knife'
  alias chef='/cygdrive/c/opscode/chefdk/embedded/bin/ruby C:/opscode/chefdk/bin/chef'
  alias chef-client='/cygdrive/c/opscode/chefdk/embedded/bin/ruby C:/opscode/chefdk/bin/chef-client'
  alias chef-solo='/cygdrive/c/opscode/chefdk/embedded/bin/ruby C:/opscode/chefdk/bin/chef-solo'
  alias shef='/cygdrive/c/opscode/chefdk/embedded/bin/ruby C:/opscode/chefdk/bin/shef'
else
  plugins=(ant bower cabal colored-man-pages cp command-not-found gitfast git-extras git-flow-avh gradle history-substring-search jsontools mvn npm nodejs scala sbt vagrant vi-mode zsh-syntax-highlighting)
fi

# User configuration
if [[ "$(expr substr $(uname -s) 1 6)" == "CYGWIN" ]] || [[ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]]; then
  #export CYGWIN=winsymlinks:nativestrict
  export PATH="$PATH:$(cygpath -pu "`reg query 'HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment' /v PATH| grep PATH | cut -c23-`")"
#  export VAGRANT_DETECTED_OS=cygwin
  export ZSH_TMUX_AUTOSTART=true
  export ZSH_TMUX_AUTOSTART_ONCE=true
  export JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.8.0
  export JAVA_HOME32=/cygdrive/c/Program\ Files\ (x86)/Java/jdk1.8.0
  proxy=rwestproxy-neurathdc.rwe.com:8080
  export http_proxy=http://$proxy
  export https_proxy=http://$proxy
fi

if [[ $(uname -s) == "Linux" ]]; then
  export NVM_DIR="/home/martin/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
fi

# export PATH="/usr/bin:/bin:/usr/sbin:/sbin:$PATH"
# export MANPATH="/usr/local/man:$MANPATH"
#export ZSH_TMUX_AUTOSTART=true
#export ZSH_TMUX_AUTOSTART_ONCE=true
source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

source ~/.sol.dark
source ~/.prompt.sh
setopt hist_ignore_all_dups
[[ -e ~/.profile ]] && emulate sh -c 'source ~/.profile'

function lights-off()
{
    ~/.sol.dark
    tmux source-file ~/.tmuxcolors-dark.conf
}

function lights-on()
{
    ~/.sol.light
    tmux source-file ~/.tmuxcolors-light.conf
}

